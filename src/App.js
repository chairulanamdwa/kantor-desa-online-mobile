import 'react-native-gesture-handler';
import React, { Component } from 'react';
import { StatusBar } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import Router from './router';
import { Provider } from 'react-redux';
import { store } from './config/redux'

class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <NavigationContainer>
          <StatusBar hidden={true} />
          <Router />
        </NavigationContainer >
      </Provider>
    );
  }
};

export default App;

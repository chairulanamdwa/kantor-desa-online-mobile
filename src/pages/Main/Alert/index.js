import React from 'react';
import { StyleSheet, View } from 'react-native';

const Alert = () => {
    return (
        <View style={styles.alert}>
        </View>
    )
}

export default Alert;


const styles = StyleSheet.create({
    alert: {
        backgroundColor: '#ddd',
        borderRadius: 20,
        height: 150,
    }
})
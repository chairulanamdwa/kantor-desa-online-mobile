import React, { Component } from 'react';
import { Image, ScrollView, StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import { Icon } from 'react-native-elements'
import { getBerita } from '../../../config/redux/action';
import { connect } from 'react-redux';
// Components
import Card from './Card';

class CardBerita extends Component {

    componentDidMount = async () => {
        await this.props.getBeritaAPI().catch(err => err);
    }

    render() {
        return (
            <View style={{ marginVertical: 30 }}>
                <Text style={{ fontSize: 12, fontWeight: 'bold' }}>Berita Desa</Text>
                <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
                    <View style={{ flexDirection: 'row' }}>
                        {this.props.isLoading ? <Text style={{ justifyContent: 'center', alignItems: 'center' }}>Mengambil Data..</Text> : this.props.listBeritaApi.map((val, key) => {
                            return <Card title={val.title} desc={val.description} img={val.urlToImage} />
                        })}
                    </View>
                </ScrollView>
            </View>
        )
    }
}


const reduxState = (state) => ({
    listBeritaApi: state.listBerita,
    isLoading: state.isLoading,
})

const reduxDispatch = (dispatch) => ({
    getBeritaAPI: (data) => dispatch(getBerita(data))
})


export default connect(reduxState, reduxDispatch)(CardBerita);


const styles = StyleSheet.create({
    card: {
        backgroundColor: 'white',
        borderRadius: 10,
        flexDirection: 'row',
        marginVertical: 20,
        padding: 15,
        width: 230,
    },
    desc: {
        fontSize: 12,
        flexWrap: 'wrap',
    },
    image: {
        backgroundColor: '#ddd',
        borderRadius: 10,
        height: 70,
        width: 70,
    },
    title: {
        fontSize: 14,
        fontWeight: 'bold',
    }
})
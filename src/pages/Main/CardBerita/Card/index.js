import React from 'react';
import { Image, ScrollView, StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import { Icon } from 'react-native-elements'

const Card = ({ desc, title, img }) => {
    return (
        <View style={styles.card}>
            <View style={styles.image}>
                <Image source={{ uri: img }} />
            </View>
            <View style={{ marginLeft: 10, width: '60%' }}>
                <Text style={styles.title}>{title}</Text>
                <Text style={styles.desc}>{desc}</Text>
            </View>
        </View>
    )
}

export default Card;


const styles = StyleSheet.create({
    card: {
        backgroundColor: 'white',
        borderRadius: 10,
        flexDirection: 'row',
        marginVertical: 20,
        marginHorizontal: 10,
        padding: 15,
        width: 230,
    },
    desc: {
        fontSize: 12,
        flexWrap: 'wrap',
    },
    image: {
        backgroundColor: '#ddd',
        borderRadius: 10,
        height: 70,
        width: 70,
    },
    title: {
        fontSize: 14,
        fontWeight: 'bold',
    }
})
import React, { Component } from 'react';
import { Image, ScrollView, StyleSheet, Text, View } from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import { getUserApi, getBerita } from '../../config/redux/action';
import { connect } from 'react-redux';
// Components
import Alert from './Alert';
import CardBerita from './CardBerita';
import CardTop from './CardTop';
import MainMenu from './MainMenu';

class Main extends Component {

    componentDidMount = async () => {
        const user = await AsyncStorage.getItem('id_user');
        const token = await AsyncStorage.getItem('access_token');

        const AuthStr = 'Bearer '.concat(token);
        await this.props.getApi({ user, AuthStr }).catch(err => err);

    }

    render() {
        return (
            <ScrollView>
                <View style={styles.container}>
                    <Text style={{ fontSize: 40, marginTop: 50, marginBottom: 30, fontFamily: 'EXO350DB' }}>Kantor Desa Online</Text>
                    <CardTop nama={this.props.nama} nik={this.props.nik} navigation={this.props.navigation} />
                    <MainMenu navigation={this.props.navigation} />
                    <Alert />

                    <CardBerita />

                </View>
            </ScrollView>
        )
    }
}

const reduxState = (state) => ({
    nama: state.user.name,
    nik: state.user.nik,
})

const reduxDispatch = (dispatch) => ({
    getApi: (data) => dispatch(getUserApi(data)),
    getBeritaAPI: (data) => dispatch(getBerita(data))
})

export default connect(reduxState, reduxDispatch)(Main);

const styles = StyleSheet.create({
    cardTop: {
        backgroundColor: '#503E9D',
        borderRadius: 15,
        padding: 10,
        position: 'relative'
    }
    , container: {
        marginHorizontal: 30,
        marginVertical: 30
    }
})

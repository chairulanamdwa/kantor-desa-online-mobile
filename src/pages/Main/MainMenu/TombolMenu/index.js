import React from 'react';
import { Image, ScrollView, StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import { info, inovasi, kontak, layanan, LogAktifitas, pengaduan } from '../../../../assets/images/icons';

const TombolMenu = ({ icon, title, ...props }) => {
    return (
        <TouchableOpacity style={styles.button} {...props}>
            <Image source={icon} style={styles.icon} />
            <Text style={styles.title}>{title}</Text>
        </TouchableOpacity>
    )
}

export default TombolMenu;


const styles = StyleSheet.create({
    button: {
        alignItems: 'center',
        backgroundColor: 'white',
        borderRadius: 20,
        elevation: 1,
        height: 90,
        justifyContent: 'center',
        marginHorizontal: 10,
        marginVertical: 10,
        shadowColor: '#000',
        width: 90,
    },
    title: {
        color: '#E29700',
        fontWeight: 'bold',
        marginTop: 5
    },
    icon: {
        height: 40,
        width: 40,
    }
})
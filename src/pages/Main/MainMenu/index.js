import React from 'react';
import { Image, ScrollView, StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import { info, inovasi, kontak, layanan, LogAktifitas, pengaduan } from '../../../assets/images/icons';

import TombolMenu from './TombolMenu';

const MainMenu = ({ navigation }) => {
    return (
        <View style={{ marginVertical: 30 }}>
            <Text style={{ fontSize: 12, fontWeight: 'bold' }}>Menu Utama</Text>
            <ScrollView contentContainerStyle={{ flexGrow: 1, justifyContent: 'center' }} horizontal={true} showsHorizontalScrollIndicator={false} >
                <View style={{ flexDirection: 'row', marginVertical: 20 }}>
                    <TombolMenu icon={layanan} title='Layanan' onPress={() => navigation.navigate('Layanan')} />
                    {/* <TombolMenu icon={inovasi} title='Inovasi' onPress={() => alert('Inovasi')} /> */}
                    <TombolMenu icon={kontak} title='Kontak' onPress={() => navigation.navigate('Kontak')} />
                    {/* <TombolMenu icon={pengaduan} title='Pengaduan' onPress={() => alert('Pengaduan')} /> */}
                    <TombolMenu icon={info} title='Info Aplikasi' onPress={() => navigation.navigate('Info')} />
                </View>
            </ScrollView>
        </View>
    )
}

export default MainMenu;


const styles = StyleSheet.create({
    button: {
        alignItems: 'center',
        backgroundColor: 'white',
        borderRadius: 20,
        elevation: 1,
        height: 90,
        justifyContent: 'center',
        marginHorizontal: 10,
        marginVertical: 10,
        shadowColor: '#000',
        width: 90,
    },
    title: {
        color: '#E29700',
        fontWeight: 'bold',
        marginTop: 5
    },
    icon: {
        height: 40,
        width: 40,
    }
})
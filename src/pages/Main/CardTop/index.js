import React from 'react';
import { Image, StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import { LogAktifitas } from '../../../assets/images/icons';
import { Human1 } from '../../../assets/images/illustrations';
const CardTop = ({ nama, nik, navigation }) => {
    return (
        <View style={styles.cardTop}>
            <Image source={Human1} style={{ position: 'absolute', right: 5, top: -80 }} />
            <View style={{ marginBottom: 30 }} >
                <Text style={{ fontSize: 24, color: '#FFFFFF' }}>{nama}</Text>
                <Text style={{ fontSize: 14, color: '#FFFFFF' }}>{nik}</Text>
            </View>
            <View style={{ flexDirection: 'row', position: 'absolute', right: 10, bottom: 10 }}>
                <TouchableOpacity onPress={() => navigation.navigate('log-aktifitas')} style={{ flexDirection: 'row' }}>
                    <Image source={LogAktifitas} />
                    <Text style={{ color: 'white', marginLeft: 10 }}>Log Aktifitas</Text>
                </TouchableOpacity>
            </View>
        </View>
    )
}

export default CardTop;


const styles = StyleSheet.create({
    cardTop: {
        backgroundColor: '#E29700',
        borderRadius: 15,
        padding: 10,
        position: 'relative'
    }
    , container: {
        marginHorizontal: 30,
        marginVertical: 30
    }
})
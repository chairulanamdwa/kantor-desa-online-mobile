import React, { useEffect } from 'react';
import { ActivityIndicator, Image, StyleSheet, Text, View } from 'react-native';

// Assets
import { pemda } from '../../assets/images/logos';

const Splash = ({ navigation }) => {
    useEffect(() => {
        setTimeout(() => {
            navigation.replace('Login')
        }, 5000)
    });
    return (
        <View style={styles.container}>
            <View style={styles.indikator}>
                <ActivityIndicator size='small' color="#E29700" />
            </View>
            <Image source={pemda} style={styles.logo} />
            <Text style={styles.title}>Kantor Desa Online</Text>
            <View style={styles.footer}>
                <Text style={{ fontSize: 12, color: '#111111' }}>Versi 1.0</Text>
            </View>
        </View>
    )
}


export default Splash;

const styles = StyleSheet.create({
    container: {
        alignItems: 'center',
        justifyContent: 'center',
        flex: 1,
        marginHorizontal: 30,
    },
    footer: {
        alignItems: 'center',
        marginTop: 50
    },
    indikator: {
        position: 'absolute',
        right: 5,
        top: 10
    },
    logo: {
        height: 150,
        marginVertical: 30,
        width: 150,
    },
    title: {
        fontSize: 35,
        fontFamily: 'EXO350DB',
    }
})
import React, { Component } from 'react';
import { Image, ScrollView, StyleSheet, Text, View, TouchableOpacity, Linking } from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import { connect } from 'react-redux';
import { loginApi } from '../../config/redux/action';
// Atoms
import { CButton, Input } from '../../components/atoms'
// Assets
import { pemda } from '../../assets/images/logos';
import axios from 'axios';

class Login extends Component {

    constructor() {
        super()
        this.state = {
            nik: '',
            password: '',
            alertNik: '',
            alertPas: '',
            alert: '',
            browser: 'https://kantordesaonline.masuk.id/register',
        }
    }

    handleSubmit = async () => {
        const res = await this.props.loginApi({ nik: this.state.nik, password: this.state.password }).catch(err => err);


        if (res) {
            AsyncStorage.setItem('id_user', this.props.idUser.toString());
            AsyncStorage.setItem('access_token', this.props.tokenUser);
            this.props.navigation.replace('Main')
        } else {
            this.setState({
                alert: 'NIK atau Password yang anda masukan salah...!'
            })
        }
    }

    componentDidMount = async () => {
        await AsyncStorage.getItem('access_token', (error, result) => {
            if (result) {
                if (this.props.isLogin) {
                    this.props.navigation.replace('Main')
                }
            }
        });
    }

    handleBrowser = () => {
        Linking.canOpenURL(this.state.browser).then(supported => {
            if (supported) {
                Linking.openURL(this.state.browser);
            } else {
                console.log("Don't know how to open URI: " + this.props.url);
            }
        });
    }

    render() {
        return (
            <ScrollView >
                <View style={styles.container}>
                    <View style={styles.header}>
                        <Image source={pemda} style={styles.logo} />
                        <Text style={styles.title}>Kantor Desa Online </Text>
                        <Text style={{ color: '#111111' }}>Untuk login, pastikan NIK anda sudah terdaftar di Kantor Desa Onlie</Text>
                    </View>
                    <View >
                        <Text style={{ color: 'red', }}>{this.state.alert}</Text>
                        <Input placeholder="Nomor Induk Kependudukan" onChangeText={(text) => this.setState({ nik: text })} />
                        <Input placeholder="Kata Sandi" onChangeText={(text) => this.setState({ password: text })} secureTextEntry={true} />

                        <View style={{ marginVertical: 10, flexDirection: 'row', flexWrap: 'wrap' }}>
                            <Text>Belum Punya Akun ?,Silahkan Registrasi melalui Website Kantor Desa Online dengan mengklik URL di Bawah </Text>
                            <TouchableOpacity onPress={() => this.handleBrowser()}>
                                <Text style={{ color: '#E29700' }}>https://kantordesaonline.masuk.id </Text>
                            </TouchableOpacity>
                        </View>
                        <CButton title="Masuk" onPress={() => this.handleSubmit()} status={this.props.isLoading} />
                    </View>
                </View>
            </ScrollView>
        )
    }
}

const reduxState = (state) => ({
    tokenUser: state.tokenUser,
    idUser: state.idUser,
    isLoading: state.isLoading,
    isLogin: state.isLogin
})

const reduxDispatch = (dispatch) => ({
    loginApi: (data) => dispatch(loginApi(data))
})

export default connect(reduxState, reduxDispatch)(Login);

const styles = StyleSheet.create({
    container: {
        marginHorizontal: 30,
        marginTop: '20%',
        marginBottom: '20%'
    },
    header: {
        marginBottom: 30
    },
    logo: {
        height: 150,
        marginVertical: 10,
        width: 150,
    },
    title: {
        fontSize: 35,
        fontFamily: 'EXO350DB',
    }
});
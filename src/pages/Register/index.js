import React, { Component } from 'react';
import { Button, Image, ScrollView, StyleSheet, Text, View, TouchableOpacity, Camera } from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import { connect } from 'react-redux';
import { loginApi } from '../../config/redux/action';
import DatePicker from 'react-native-datepicker';
import ImagePicker from 'react-native-image-picker';
// Atoms
import { CButton, Input } from '../../components/atoms'
// Assets
import { pemda, defaultGambar } from '../../assets/images/logos';
import axios from 'axios';

class Register extends Component {

    constructor() {
        super()
        this.state = {
            nik: '',
            nama: '',
            date: new Date(),
            password: '',
            alertNik: '',
            alertPas: '',
            alert: '',
            filePath: {},
            pic: '',
            data: {}
        }
    }

    chooseFile = () => {
        const options = {
            title: 'Select Image',
            customButtons: [
                { name: 'customOptionKey', title: 'Choose Photo from Custom Option' },
            ],
            storageOptions: {
                skipBackup: true,
                path: 'images',
            },
        };

        ImagePicker.showImagePicker(options, response => {
            console.log('Response = ', response);

            if (response.didCancel) {
                console.log('User cancelled image picker');
            } else if (response.error) {
                console.log('ImagePicker Error: ', response.error);
            } else if (response.customButton) {
                console.log('User tapped custom button: ', response.customButton);
                alert(response.customButton);
            } else {
                let source = response;
                // You can also display the image using data:
                // let source = { uri: 'data:image/jpeg;base64,' + response.data };
                this.setState({
                    filePath: source,
                    pic: response,
                });
            }
        });

    };

    handleSubmit = async () => {

        // console.warn(this.state.pic.path);
        const config = { headers: { "Content-Type": "multipart/form-data" } };
        const data = new FormData();
        // data.append('nama', this.state.nama);
        // data.append('nik', this.state.nik);
        // data.append('date', this.state.date);
        // data.append('file', 'test.jpg');
        data.append("image", {
            uri: this.state.pic.uri,
            type: 'image/jpeg',
            name: 'image'
        });
        console.warn(data);
        await axios.post(`https://kantordesaonline.masuk.id/api/v1/user/register`, {
            'image': [{
                uri: this.state.pic,
                type: 'image/jpeg',
                name: 'image'
            }]
        }).then(res => {
            console.warn(res);
            // alert(res)
        }).catch(
            function (err) {
                console.warn(err);
            }
        )
    }


    render() {
        return (
            <ScrollView >
                <View style={styles.container}>
                    <View style={styles.header}>
                        <Image source={pemda} style={styles.logo} />
                        <Text style={styles.title}>Kantor Desa Online </Text>
                        <Text style={{ color: '#111111' }}>Registrasikan diri anda di Aplikasi Kantor Desa Online</Text>
                    </View>
                    <View >
                        <Text style={{ color: 'red', }}>{this.state.alert}</Text>
                        <Input placeholder="Nama Lengkap" onChangeText={(text) => this.setState({ nama: text })} />
                        <Input placeholder="Nomor Induk Kependudukan" onChangeText={(text) => this.setState({ nik: text })} />
                        <DatePicker
                            style={styles.inpurDate}
                            date={this.state.date}
                            mode="date"
                            format="YYYY-MM-DD"
                            minDate="2016-05-01"
                            confirmBtnText="Confirm"
                            cancelBtnText="Cancel"
                            customStyles={{
                                dateIcon: {
                                    position: 'absolute',
                                    left: 0,
                                    top: 4,
                                    marginLeft: 0
                                },
                                dateInput: {
                                    marginLeft: 36
                                }
                                // ... You can check the source to find the other keys.
                            }}
                            onDateChange={(date) => { this.setState({ date: date }) }}
                        />
                        <Text>
                            Pilih Gambar KTP
                        </Text>
                        <TouchableOpacity onPress={() => this.chooseFile()} style={{ backgroundColor: '#ccc' }}>
                            <Image source={this.state.filePath != {} ? this.state.filePath : defaultGambar} style={{ width: '100%', height: 180 }} />
                        </TouchableOpacity>


                        <View style={{ marginVertical: 10, flexDirection: 'row' }}>
                            <Text>Sudah Punya Akun,Silahkan </Text>
                            <TouchableOpacity onPress={() => this.props.navigation.navigate('Login')}>
                                <Text style={{ color: '#E29700' }}>Login</Text>
                            </TouchableOpacity>
                        </View>
                        <CButton title="Registrasi" onPress={() => this.handleSubmit()} status={this.props.isLoading} />
                    </View>
                </View>
            </ScrollView>
        )
    }
}

const reduxState = (state) => ({
    tokenUser: state.tokenUser,
    idUser: state.idUser,
    isLoading: state.isLoading,
    isLogin: state.isLogin
})

const reduxDispatch = (dispatch) => ({
    loginApi: (data) => dispatch(loginApi(data))
})

export default connect(reduxState, reduxDispatch)(Register);

const styles = StyleSheet.create({
    container: {
        marginHorizontal: 30,
        marginTop: '20%',
        marginBottom: '20%'
    },
    header: {
        marginBottom: 30
    },
    logo: {
        height: 150,
        marginVertical: 10,
        width: 150,
    },
    title: {
        fontSize: 35,
        fontFamily: 'EXO350DB',
    },
    inpurDate: {
        height: 40,
        marginVertical: 20,
        width: '50%',
    }
});
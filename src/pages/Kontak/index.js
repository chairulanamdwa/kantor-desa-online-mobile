import React, { Component } from 'react';
import { ScrollView, StyleSheet, Text, View, Linking } from 'react-native';
import { ListItem } from 'react-native-elements';
// Component
import { connect } from 'react-redux';
import { getKontak } from '../../config/redux/action';
import Icon from 'react-native-vector-icons/FontAwesome';
class Kontak extends Component {

    componentDidMount = async () => {

        await this.props.getKontakAPI().catch(err => err);

    }

    handlePhone = (data) => {
        Linking.openURL(`tel:${data}`)
    }

    render() {
        if (this.props.isLoading) {
            return (
                <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                    <Text>Harap Tunggu...</Text>
                </View>
            )
        } else {
            return (
                <ScrollView>
                    <View style={styles.container}>
                        <Text style={{ fontSize: 40, marginTop: 10, marginBottom: 30, fontFamily: 'EXO350DB' }}>Kontak Penting</Text>
                        {this.props.isLoading ? <Text>Sedang Mengambil Data...</Text> : this.props.listKontak.map((val, key) => {
                            return <ListItem
                                onPress={() => this.handlePhone(val.telpon)}
                                key={key}
                                title={val.nama_kontak}
                                subtitle={val.telpon}
                                leftAvatar={{ source: { uri: 'http://kantordesaonline.masuk.id/templates/backend/img/kontak/' + val.gambar } }}
                                rightIcon={<Icon name="phone" size={30} color={'#E29700'} />}
                                bottomDivider
                                style={{ marginTop: 5 }}
                            />
                        })}
                    </View>
                </ScrollView>
            )
        }
    }
}


const reduxState = (state) => ({
    listKontak: state.listKontak,
    isLoading: state.isLoading
})
const reduxDispatch = (dispatch) => ({
    getKontakAPI: () => dispatch(getKontak())
})

export default connect(reduxState, reduxDispatch)(Kontak);


const styles = StyleSheet.create({
    container: {
        marginHorizontal: 30,
        marginVertical: 30
    }
})
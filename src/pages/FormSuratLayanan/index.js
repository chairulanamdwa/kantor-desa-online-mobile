import SuratNikah from './SuratNikah';
import SuratUsaha from './SuratUsaha';
import SuratKehilangan from './SuratKehilangan';
import SuratDomisili from './SuratDomisili';
import SuratSanda from './SuratSanda';
import SuratJualPutus from './SuratJualPutus';
import SuratPenguasaanFisikTanah from './SuratPenguasaanFisikTanah';


export {
    SuratNikah,
    SuratUsaha,
    SuratKehilangan,
    SuratDomisili,
    SuratSanda,
    SuratJualPutus,
    SuratPenguasaanFisikTanah
}
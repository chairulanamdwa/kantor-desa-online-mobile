import React from 'react';
import { Text, TouchableOpacity, Linking, View } from 'react-native';

const SuratPenguasaanFisikTanah = ({ navigation }) => {
    return (
        <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
            <Text>Menu layanan hanya bisa di akses melalui Website Kantor Desa Online di bawah ini</Text>
            <TouchableOpacity onPress={() => Linking.openURL('http://kantordesaonline.masuk.id/change-surat-pelayanan-online')}>
                <Text style={{ color: '#E29700' }}>https://kantordesaonline.masuk.id </Text>
            </TouchableOpacity>
        </View>
    )
}


export default SuratPenguasaanFisikTanah;
import React, { Component } from 'react';
import { Text, View, InputGroup } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import { CButton, Input, InputDisabled } from '../../../components/atoms';
import { connect } from 'react-redux';
import { getNIKAPI } from '../../../config/redux/action'
import { PostSuratUsaha } from '../../../config/redux/action/Surat'
class SuratUsaha extends Component {


    constructor({ idSurat }) {
        super()
        this.state = {
            nik: '',
            id_surat: '',
            usaha: '',
            id_user: '',
            status: false,
            color: '#E29700',
            alert: '',
        }
    }


    autoComplite = async () => {
        const status = await this.props.getPendudukAPI({ nik: this.state.nik }).catch(err => err);
        if (status) {
            this.setState({
                status: true,
            });
        }

        if (this.props.nama) {
            this.setState({
                color: '#08c217',
                alert: 'check'
            });
        } else {
            this.setState({
                color: '#f72020',
                alert: 'times'
            });
        }

    }

    handleSubmit = async () => {

        if (this.state.usaha == '') {
            return alert('Usaha Masih Kosong');
        }
        if (this.props.id_penduduk == '') {
            return alert('ID Penduduk Masih Kosong');
        }

        const res = await this.props.PostSuratUsaha({ id_penduduk1: this.props.id_penduduk, usaha: this.state.usaha, id_user: this.props.id_user }).catch(err => err);

        if (this.props.code == '200') {
            alert(this.props.message);
        } else {
            alert(this.props.message);
        }

    }




    render() {

        return (
            <View style={{ flex: 1, alignItems: 'center', padding: 30 }}>
                <View style={{ backgroundColor: 'white', width: '100%', paddingHorizontal: 30, paddingVertical: 20, marginBottom: 20 }}>

                    <View>
                        <Input placeholder="NIK" onChangeText={(nik) => this.setState({ nik: nik })} colorBorder={this.state.color} />
                    </View>
                    <CButton title="Cari NIK" onPress={() => this.autoComplite()} />
                </View>
                <View style={{ marginTop: 30, width: '100%' }}>
                    <InputDisabled placeholder="Jenis Surat" value='surat-usaha' editable={false} />
                    <InputDisabled placeholder="ID Nama" value={this.props.id_penduduk} editable={false} />
                    <InputDisabled placeholder="Nama" value={this.props.nama} editable={false} />
                    <InputDisabled placeholder="Nomor Kartu Keluarga" value={this.props.kk} editable={false} />
                    <Input placeholder="Nama Usaha" onChangeText={(text) => this.setState({ usaha: text })} />
                </View>
                <View style={{ position: 'absolute', bottom: 10, width: '100%' }}>
                    <CButton title="Ajukan Pembuatan Surat" onPress={() => this.handleSubmit()} status={this.props.isLoading} />
                </View>
            </View>
        )
    }
}

const reduxState = (state) => ({
    nama: state.penduduk.nama,
    kk: state.penduduk.kk,
    id_penduduk: state.penduduk.id_penduduk,
    isLoading: state.isLoading,
    id_user: state.user.id,
    code: state.callbackSurat.code,
    message: state.callbackSurat.message
})
const reduxDispatch = (dispatch) => ({
    PostSuratUsaha: (data) => dispatch(PostSuratUsaha(data)),
    getPendudukAPI: (data) => dispatch(getNIKAPI(data))
})

export default connect(reduxState, reduxDispatch)(SuratUsaha);
import React, { Component } from 'react';
import { Text, ScrollView, View } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import { CButton, Input, InputDisabled, Textarea } from '../../../components/atoms';
import { connect } from 'react-redux';
import { PostSuratKehilangan } from '../../../config/redux/action/Surat'
import { getNIKAPI } from '../../../config/redux/action'
class SuratKehilangan extends Component {


    constructor({ idSurat }) {
        super()
        this.state = {
            nik: '',
            id_surat: '',
            barang: '',
            tmpt_hilang: '',
            menuju: '',
            telpon: '',
            id_user: '',
            status: false,
            color: '#E29700',
            alert: '',
        }
    }


    autoComplite = async () => {
        const status = await this.props.getPendudukAPI({ nik: this.state.nik }).catch(err => err);
        if (status) {
            this.setState({
                status: true,
            });
        }

        if (this.props.nama) {
            this.setState({
                color: 'green',
                alert: 'check'
            });
        } else {
            this.setState({
                color: 'red',
                alert: 'times'
            });
        }

    }

    handleSubmit = async () => {
        if (this.state.barang == '') {
            return alert('Barang Masih Kosong');
        }
        if (this.state.tmpt_hilang == '') {
            return alert('Tempat Hilang Masih Kosong');
        }
        if (this.state.menuju == '') {
            return alert('Menuju Masih Kosong');
        }
        if (this.state.telpon == '') {
            return alert('Telpon Masih Kosong');
        }
        if (this.props.id_penduduk == '') {
            return alert('ID Penduduk Masih Kosong');
        }

        await this.props.PostSuratKehi({ id_penduduk1: this.props.id_penduduk, barang: this.state.barang, tmpt_hilang: this.state.tmpt_hilang, menuju: this.state.menuju, telpon: this.state.telpon, id_user: this.props.id_user }).catch(err => err);

        if (this.props.code == '200') {
            alert(this.props.message);
        } else {
            alert(this.props.message);
        }
    }



    render() {

        return (
            <ScrollView>
                <View style={{ flex: 1, alignItems: 'center', padding: 30 }}>
                    <View style={{ backgroundColor: 'white', width: '100%', paddingHorizontal: 30, paddingVertical: 20, marginBottom: 20 }}>

                        <View >
                            <Input placeholder="NIK" onChangeText={(nik) => this.setState({ nik: nik })} colorBorder={this.state.color} />
                        </View>
                        <CButton title="Cari NIK" onPress={() => this.autoComplite()} />
                    </View>
                    <View style={{ marginTop: 30, width: '100%' }}>
                        <InputDisabled placeholder="Jenis Surat" value='surat-kehilangan' editable={false} />
                        <InputDisabled placeholder="ID Nama" value={this.props.id_penduduk} editable={false} />
                        <InputDisabled placeholder="Nama" value={this.props.nama} editable={false} />
                        <InputDisabled placeholder="Nomor Kartu Keluarga" value={this.props.kk} editable={false} />
                        <Input placeholder="Barang yang hilang" onChangeText={(text) => this.setState({ barang: text })} />
                        <Input placeholder="Tempat hilang" onChangeText={(text) => this.setState({ tmpt_hilang: text })} />
                        <Textarea placeholder="Arah hilang/Menuju arah" onChangeText={(text) => this.setState({ menuju: text })} />
                        <Input placeholder="Nomor Hp (Aktif)" onChangeText={(text) => this.setState({ telpon: text })} />

                    </View>
                    <View style={{ width: '100%' }}>
                        <CButton title="Ajukan Pembuatan Surat" onPress={() => this.handleSubmit()} status={this.props.isLoading} />
                    </View>
                </View>
            </ScrollView>
        )
    }
}

const reduxState = (state) => ({
    nama: state.penduduk.nama,
    id_user: state.user.id,
    kk: state.penduduk.kk,
    id_penduduk: state.penduduk.id_penduduk,
    isLoading: state.isLoading,
    code: state.callbackSurat.code,
    message: state.callbackSurat.message
})
const reduxDispatch = (dispatch) => ({
    getPendudukAPI: (data) => dispatch(getNIKAPI(data)),
    PostSuratKehi: (data) => dispatch(PostSuratKehilangan(data))
})

export default connect(reduxState, reduxDispatch)(SuratKehilangan);
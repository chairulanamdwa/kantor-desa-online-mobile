import React, { Component } from 'react';
import { Text, ScrollView, TouchableOpacity, View, Linking } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import { Button, Input, InputDisabled, Textarea } from '../../../components/atoms';
import { connect } from 'react-redux';
import { PostSuratSanda } from '../../../config/redux/action/Surat';
import { getNIKAPI } from '../../../config/redux/action'
class SuratSanda extends Component {


    constructor({ idSurat }) {
        super()
        this.state = {
            nik1: '',
            nik2: '',
            nik3: '',
            nik4: '',
            status: false,
            color: '#E29700',
            alert: '',
        }
    }


    autoComplite = async () => {
        const status1 = await this.props.getPendudukAPI({ nik: this.state.nik1 }).catch(err => err);
        const status2 = await this.props.getPendudukAPI({ nik2: this.state.nik2 }).catch(err => err);
        const status3 = await this.props.getPendudukAPI({ nik3: this.state.nik3 }).catch(err => err);
        const status4 = await this.props.getPendudukAPI({ nik4: this.state.nik4 }).catch(err => err);
        if (status1) {
            this.setState({
                status: true,
            });
        }

        if (this.props.nama) {
            this.setState({
                color: '#08c217',
                alert: 'check'
            });
        } else {
            this.setState({
                color: '#f72020',
                alert: 'times'
            });
        }

    }

    handleSubmit = () => {
        alert(this.props.nama + ' No KK ' + this.state.idSurat);
    }



    render() {

        return (
            <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                <Text>Menu layanan hanya bisa di akses melalui Website Kantor Desa Online di bawah ini</Text>
                <TouchableOpacity onPress={() => Linking.openURL('http://kantordesaonline.masuk.id/change-surat-pelayanan-online')}>
                    <Text style={{ color: '#E29700' }}>https://kantordesaonline.masuk.id </Text>
                </TouchableOpacity>
            </View>
            // <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center', }}>
            //     {/* <View style={{ width: '100%' }}>
            //             <View style={{ backgroundColor: 'white', paddingHorizontal: 30, paddingVertical: 20, marginBottom: 20 }}>
            //                 <View>
            //                     <Text>Penjual</Text>
            //                     <Input placeholder="NIK" onChangeText={(nik) => this.setState({ nik1: nik })} colorBorder={this.state.color} />
            //                     <Text>Pembeli</Text>
            //                     <Input placeholder="NIK" onChangeText={(nik) => this.setState({ nik2: nik })} />
            //                     <Text>Saksi 1</Text>
            //                     <Input placeholder="NIK" onChangeText={(nik) => this.setState({ nik3: nik })} />
            //                     <Text>Saksi 2</Text>
            //                     <Input placeholder="NIK" onChangeText={(nik) => this.setState({ nik4: nik })} />
            //                 </View>
            //                 <View style={{ width: '40%' }}>
            //                     <Button title="Cari NIK" onPress={() => this.autoComplite()} status={this.props.isLoading} />
            //                 </View>
            //             </View>
            //         </View>
            //         <View style={{ marginTop: 30, width: '100%' }}>
            //             <InputDisabled placeholder="Jenis Surat" value='surat-domisili' editable={false} />
            //             <InputDisabled placeholder="Nama Penjual" value={this.props.id_penduduk} editable={false} />
            //             <InputDisabled placeholder="Nama Pembeli" value={this.props.id_penduduk} editable={false} />
            //             <InputDisabled placeholder="Nama Saksi 1" value={this.props.id_penduduk} editable={false} />
            //             <InputDisabled placeholder="Nama Saksi 2" value={this.props.id_penduduk} editable={false} />

            //         </View>
            //         <View style={{ width: '100%' }}>
            //             <Button title="Ajukan Pembuatan Surat" onPress={() => this.handleSubmit()} />
            //         </View> */}
            //     <Text style={{ fontSize: 20 }}>Akan Segera Hadir</Text>
            // </View>
        )
    }
}

const reduxState = (state) => ({
    nama: state.penduduk.nama,
    kk: state.penduduk.kk,
    id_penduduk: state.penduduk.id_penduduk,
    isLoading: state.isLoading
})
const reduxDispatch = (dispatch) => ({
    getPendudukAPI: (data) => dispatch(getNIKAPI(data))
})

export default connect(reduxState, reduxDispatch)(SuratSanda);
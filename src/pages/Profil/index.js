import React, { Component } from 'react';
import { Image, StyleSheet, Text, View } from 'react-native';
import CButton from '../../components/atoms/Button';
import AsyncStorage from '@react-native-community/async-storage';
import { getUserApi } from '../../config/redux/action';
import { connect } from 'react-redux';

// Assets
import { avatar1 } from '../../assets/images/avatar';

class Profil extends Component {

    componentDidMount = async () => {
        const user = await AsyncStorage.getItem('id_user');
        const token = await AsyncStorage.getItem('access_token');
        const AuthStr = 'Bearer '.concat(token);
        await this.props.UserApi({ user, AuthStr }).catch(err => err);
    }

    render() {
        const logout = () => {
            AsyncStorage.removeItem('id_user');
            AsyncStorage.removeItem('access_token');
            this.props.navigation.replace('Login')
        }
        return (
            <View>
                <View style={{ backgroundColor: 'white' }}>
                    <View style={styles.container}>
                        <View style={{ marginVertical: 80, alignItems: 'center', justifyContent: 'center' }}>
                            <Image source={avatar1} style={{ width: 150, height: 150 }} />
                            <Text style={{ fontSize: 28, marginTop: 20 }}>{this.props.nama}</Text>
                            <Text style={{ fontSize: 20 }}>{this.props.nik}</Text>
                        </View>
                    </View>
                </View>
                <View style={styles.container}>
                    <CButton title="Logout" onPress={() => logout()} />
                </View>
            </View>
        )
    }
}

const reduxState = (state) => ({
    nama: state.user.name,
    nik: state.user.nik,
})
const reduxDispatch = (dispatch) => ({
    UserApi: (data) => dispatch(getUserApi(data))
})

export default connect(reduxState, reduxDispatch)(Profil);

const styles = StyleSheet.create({
    container: {
        alignItems: 'center',
        marginHorizontal: 30,
    }
})
import Berita from './Berita';
import Layanan from './Layanan';
import Login from './Login';
import Main from './Main';
import Pegawai from './Pegawai';
import Profil from './Profil';
import Splash from './Splash';
import LogActivitas from './LogActivity';
import Register from './Register';
import Kontak from './Kontak';
import Info from './Info';

// Surat-Surat
import { SuratNikah, SuratUsaha, SuratKehilangan, SuratDomisili, SuratSanda, SuratJualPutus, SuratPenguasaanFisikTanah } from './FormSuratLayanan';

export {
    Berita, Layanan, Login, Register, LogActivitas, Main, Pegawai, Profil, Splash, SuratNikah, SuratUsaha, SuratKehilangan, SuratDomisili, SuratSanda, SuratJualPutus, SuratPenguasaanFisikTanah, Kontak, Info
}
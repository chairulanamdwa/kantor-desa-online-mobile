import React, { Component } from 'react';
import { Image, ScrollView, StyleSheet, Text, View, Linking } from 'react-native';
import { ListItem } from 'react-native-elements';
// Component
import { pemda } from '../../assets/images/logos';
import { connect } from 'react-redux';
import Icon from 'react-native-vector-icons/FontAwesome';
class Info extends Component {

    componentDidMount = async () => {


    }

    render() {
        return (
            <ScrollView style={{ backgroundColor: '#fff' }}>
                <View style={{ flex: 1, alignItems: 'center' }}>
                    <Image source={pemda} style={styles.logo} />
                    <Text style={{ color: '#E29700', fontSize: 24 }}>Aplikasi Kantor Desa Onlie</Text>
                </View>

                <View style={styles.container}>
                    <ListItem
                        title="Layanan"
                        subtitle="Sistem ini untuk mempermudah Pelayanan Pembuatan Surat agar lebih cepat."
                    />
                    <ListItem
                        title="Kontak"
                        subtitle="Kontak adalah Menu Informasi Nomor Telpon Kantor Desa Maupan Kantor-Kantor Lainnya"
                    />
                    <ListItem
                        title="Berita"
                        subtitle="Menu berita mengambil data dari Indonesia news API"
                    />
                </View>
            </ScrollView>
        )
    }
}



export default Info;


const styles = StyleSheet.create({
    container: {
        marginHorizontal: 30,
        marginVertical: 30
    }, logo: {
        height: 150,
        marginVertical: 10,
        width: 150,
    },

})
import React, { Component } from 'react';
import { ScrollView, StyleSheet, Text, View } from 'react-native';
import { GetAktifitas, getUserApi } from '../../config/redux/action';
import { connect } from 'react-redux';
import { ListItem } from 'react-native-elements';
import AsyncStorage from '@react-native-community/async-storage';
class LogActivitas extends Component {


    componentDidMount = async () => {
        const user = await AsyncStorage.getItem('id_user');
        const token = await AsyncStorage.getItem('access_token');
        const AuthStr = 'Bearer '.concat(token);
        await this.props.getApi({ user, AuthStr }).catch(err => err);
        await this.props.LogAkt({ id_user: user }).catch(err => err);
    }

    render() {

        return (
            <ScrollView>
                <View style={styles.container}>
                    {this.props.isLoading ? <Text>Sedang Mengambil Data...</Text> : this.props.aktifitas.map((val, key) => {
                        return <ListItem
                            key={key}
                            title={val.description}
                            subtitle={val.created_at}
                            bottomDivider
                            style={{ marginTop: 5 }}
                        />
                    })}
                </View>
            </ScrollView>
        );
    }
}


const reduxState = (state) => ({
    id_user: state.user.id,
    isLoading: state.isLoading,
    aktifitas: state.logAktifitas
})
const reduxDispatch = (dispatch) => ({
    LogAkt: (data) => dispatch(GetAktifitas(data)),
    getApi: (data) => dispatch(getUserApi(data)),
})

export default connect(reduxState, reduxDispatch)(LogActivitas);


const styles = StyleSheet.create({
    container: {
        marginHorizontal: 30,
        marginVertical: 30,
        flex: 1
    }
})
import React, { Component } from 'react';
import { ScrollView, StyleSheet, Text, View } from 'react-native';
import { ListItem } from 'react-native-elements';
import { getBerita } from '../../config/redux/action';
import { connect } from 'react-redux';
class Berita extends Component {

    componentDidMount = async () => {
        await this.props.getBeritaApi().catch(err => err);
    }
    render() {
        if (this.props.isLoading) {
            return (
                <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                    <Text>Sedang Mengambil Data API...</Text>
                </View>
            )
        } else {
            return (
                <ScrollView>
                    <View style={{ marginVertical: 20, marginHorizontal: 50 }}>
                        <Text style={{ fontSize: 34, fontFamily: 'EXO350DB', color: '#E29700' }}>Berita Indonesia</Text>
                    </View>
                    <View>
                        {this.props.isLoading ? <Text>Sedang Mengambil Data...</Text> : this.props.listBerita.map((val, key) => {
                            return <ListItem
                                key={key}
                                title={val.title}
                                subtitle={val.description}
                                leftAvatar={{ source: { uri: val.urlToImage } }}
                                // rightIcon={<Icon name="phone" size={30} color={'#E29700'} />}
                                bottomDivider
                                style={{ marginTop: 5 }}
                            />
                        })}
                    </View>
                </ScrollView >
            )
        }
    }
}


const reduxState = (state) => ({
    listBerita: state.listBerita,
    isLoading: state.isLoading
})
const reduxDispatch = (dispatch) => ({
    getBeritaApi: () => dispatch(getBerita())
})

export default connect(reduxState, reduxDispatch)(Berita);

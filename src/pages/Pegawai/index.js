import React, { Component, useState } from 'react';
import { ScrollView, StyleSheet, Text, TextInput, TouchableOpacity, View } from 'react-native';
import DatePicker from 'react-native-datepicker';
import { PostAktifitasKaryawan, getUserApi, ShowAktifitasKaryawan, AjukanAktifitasKaryawan, DeleteAktifitasKaryawan } from '../../config/redux/action';
import { connect } from 'react-redux';
import { ListItem } from 'react-native-elements'
import AsyncStorage from '@react-native-community/async-storage';
//  Atoms
import { CButton, Input, InputDate, Textarea } from '../../components/atoms';

class Pegawai extends Component {

    constructor(props) {
        super(props)
        this.state = {
            jenis: '',
            aktifitas: '',
            date: new Date(),
        }

    }

    componentDidMount = async () => {
        this.ApiCall();

    }

    handleSubmit = async () => {
        const user = await AsyncStorage.getItem('id_user');
        if (this.state.jenis == '') {
            return alert('Jenis Aktifitas Masih Kosong');
        }
        if (this.state.aktifitas == '') {
            return alert('Aktifitas Masih Kosong');
        }
        const res = await this.props.karyawanAktifitas({ jenis_aktifitas: this.state.jenis, aktifitas: this.state.aktifitas, tanggal: this.state.date, id_user: user }).catch(err => err);

        if (res) {
            this.ApiCall();
        }

    }

    ApiCall = async () => {
        const user = await AsyncStorage.getItem('id_user');
        const token = await AsyncStorage.getItem('access_token');
        const AuthStr = 'Bearer '.concat(token);
        await this.props.getApi({ user, AuthStr }).catch(err => err);

        await this.props.showAktifitas({ id_user: user }).catch(err => err);
    }


    handleAjukan = async () => {
        const user = await AsyncStorage.getItem('id_user');
        const res = await this.props.ajukanAktifitas({ id_user: user }).catch(err => err);
        this.ApiCall();

    }

    deleteAjuan = async (data) => {
        const user = await AsyncStorage.getItem('id_user');
        const res = await this.props.DeleteAktifitas({ id_aktifitas: data, id_user: user }).catch(err => err);
        this.ApiCall();

    }

    render() {
        if (this.props.role != 'user') {
            return (
                <ScrollView>
                    <View style={{ height: 1000, }}>
                        <View style={styles.header}>
                            <Text style={{ fontSize: 40, marginTop: 50, marginBottom: 30, fontFamily: 'EXO350DB' }}>Aktifitas Perangkat Desa</Text>
                            <Input placeholder="Jenis Aktifitas" onChangeText={(text) => this.setState({ jenis: text })} />
                            <Textarea placeholder="Aktifitas" onChangeText={(text) => this.setState({ aktifitas: text })} />
                            <DatePicker
                                style={styles.inpurDate}
                                date={this.state.date}
                                mode="date"
                                format="YYYY-MM-DD"
                                minDate="2016-05-01"
                                confirmBtnText="Confirm"
                                cancelBtnText="Cancel"
                                customStyles={{
                                    dateIcon: {
                                        position: 'absolute',
                                        left: 0,
                                        top: 4,
                                        marginLeft: 0
                                    },
                                    dateInput: {
                                        marginLeft: 36
                                    }
                                    // ... You can check the source to find the other keys.
                                }}
                                onDateChange={(date) => { this.setState({ date: date }) }}
                            />
                            <CButton title="Tambahkan" onPress={() => this.handleSubmit()} status={this.props.isLoading} />
                        </View>
                        <View style={styles.footer}>
                            {this.props.isLoading ? <Text>Membaca Data...</Text> : this.props.aktifitas.map((val, key) => {
                                return <ListItem
                                    key={key}
                                    title={val.jenis_aktifitas}
                                    subtitle={val.aktifitas}
                                    bottomDivider
                                    onLongPress={() => this.deleteAjuan(val.id_aktifitas)}
                                    style={{ width: '90%', marginHorizontal: 20, marginVertical: 2 }}
                                />
                            })}

                        </View>
                        <View style={{
                            alignItems: 'center',
                            justifyContent: 'center',
                            paddingHorizontal: 100
                        }}>
                            <CButton title="Ajukan" onPress={() => this.handleAjukan()} />
                        </View>
                    </View>
                </ScrollView>
            )
        } else {
            return (
                <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                    <Text style={{ fontSize: 20 }}>Hanya Untuk Akun Pegawai</Text>
                </View>
            )
        }
    }

}

const reduxState = (state) => ({
    isLoading: state.isLoading,
    id_user: state.user.id,
    role: state.user.role,
    aktifitas: state.aktifitasKaryawan
})

const reduxDispatch = (dispatch) => ({
    karyawanAktifitas: (data) => dispatch(PostAktifitasKaryawan(data)),
    getApi: (data) => dispatch(getUserApi(data)),
    ajukanAktifitas: (data) => dispatch(AjukanAktifitasKaryawan(data)),
    showAktifitas: (data) => dispatch(ShowAktifitasKaryawan(data)),
    DeleteAktifitas: (data) => dispatch(DeleteAktifitasKaryawan(data)),
})


export default connect(reduxState, reduxDispatch)(Pegawai);


const styles = StyleSheet.create({
    container: {
        marginHorizontal: 30,
        marginVertical: 30,
        backgroundColor: '#000'
    },
    footer: {
        alignItems: 'center',
        // backgroundColor: '#FFFFFF',
        borderTopRightRadius: 50,
        borderTopLeftRadius: 50,
        flex: 1,
        justifyContent: 'center',
        marginBottom: 50
    },
    header: {
        alignItems: 'center',
        flex: 2,
        justifyContent: 'center',
        paddingHorizontal: 50,
    },
    inpurDate: {
        height: 40,
        marginVertical: 20,
        width: '100%',
    }
})
import React, { Component } from 'react';
import { ScrollView, StyleSheet, Text, View } from 'react-native';

// Component
import CardSurat from './CardSurat';
import { connect } from 'react-redux';
import { getJenisSurat } from '../../config/redux/action';

class Layanan extends Component {

    componentDidMount = async () => {

        await this.props.getJenisSurat().catch(err => err);

    }

    render() {
        if (this.props.isLoading) {
            return (
                <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', backgroundColor: '#fff' }}>
                    <Text>Harap Tunggu...</Text>
                </View>
            )
        } else {
            return (
                <ScrollView>
                    <View style={styles.container}>
                        <Text style={{ fontSize: 40, marginTop: 10, marginBottom: 30, fontFamily: 'EXO350DB' }}>Jenis Surat</Text>
                        {this.props.listSurat.map((val, key) => {
                            return <CardSurat key={key} title={val.jenis_surat} onPress={() => this.props.navigation.navigate(val.url, { user: 'Yusuf' })} />
                        })}

                    </View>
                </ScrollView>
            )
        }
    }
}


const reduxState = (state) => ({
    listSurat: state.listSurat,
    isLoading: state.isLoading
})
const reduxDispatch = (dispatch) => ({
    getJenisSurat: () => dispatch(getJenisSurat())
})

export default connect(reduxState, reduxDispatch)(Layanan);


const styles = StyleSheet.create({
    container: {
        marginHorizontal: 30,
        marginVertical: 30
    }
})
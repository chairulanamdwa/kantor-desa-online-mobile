import React from 'react';
import { Image, ScrollView, StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import { layanan } from '../../../assets/images/icons';

const CardSurat = ({ title, ...props }) => {
    return (
        <TouchableOpacity {...props} style={styles.card}>
            <View style={{ marginRight: 10 }}>
                <Image source={layanan} />
            </View>
            <View style={{ paddingRight: 50 }}>
                <Text style={{ fontSize: 20 }}>
                    {title}
                </Text>
            </View>
        </TouchableOpacity>
    )
}

export default CardSurat;


const styles = StyleSheet.create({
    card: {
        alignItems: 'center',
        backgroundColor: '#FFFFFF',
        borderRadius: 20,
        elevation: 5,
        flexDirection: 'row',
        marginVertical: 10,
        padding: 20,
        shadowColor: '#503E9D',
    }
})
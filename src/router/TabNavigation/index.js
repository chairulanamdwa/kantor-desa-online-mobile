import React from 'react';
import { createMaterialBottomTabNavigator } from '@react-navigation/material-bottom-tabs';
import Icon from 'react-native-vector-icons/FontAwesome';
// Pages
import { Berita, Main, Pegawai, Profil, } from '../../pages';
const Tab = createMaterialBottomTabNavigator();
const MainNavigatorTab = ({ navigation }) => {
    return (
        <Tab.Navigator
            initialRouteName="Main"
            activeColor="#E29700"
            inactiveColor="#EBC883"
            barStyle={{ backgroundColor: 'white' }}
        >
            <Tab.Screen
                name="Main"
                component={Main}
                options={{
                    tabBarLabel: 'Main',
                    tabBarIcon: ({ color }) => (
                        <Icon name="home" color={color} size={21} />
                    ),
                }}
            />
            <Tab.Screen
                name="Pegawai"
                component={Pegawai}
                options={{
                    tabBarLabel: 'Pegawai',
                    tabBarIcon: ({ color }) => (
                        <Icon name="user" color={color} size={21} />
                    ),
                }}
            />
            <Tab.Screen
                name="Berita"
                component={Berita}
                options={{
                    tabBarLabel: 'Berita',
                    tabBarIcon: ({ color }) => (
                        <Icon name="newspaper-o" color={color} size={21} />
                    ),
                }}
            />
            <Tab.Screen
                name="Profil"
                component={Profil}
                options={{
                    tabBarLabel: 'Profil',
                    tabBarIcon: ({ color }) => (
                        <Icon name="user-circle" color={color} size={20} />
                    ),
                }}
            />
        </Tab.Navigator>
    )
}

export default MainNavigatorTab;
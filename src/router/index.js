import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import MainNavigatorTab from './TabNavigation';
// Pages
import { Layanan, Login, Register, LogActivitas, Splash, SuratNikah, SuratUsaha, SuratKehilangan, SuratDomisili, SuratSanda, SuratJualPutus, SuratPenguasaanFisikTanah, Kontak, Info } from '../pages';

const Stack = createStackNavigator();

const Router = () => {
    return (
        <Stack.Navigator>
            <Stack.Screen name="Splash" component={Splash} options={{ headerShown: false }} />
            <Stack.Screen name="Login" component={Login} options={{ headerShown: false }} />
            <Stack.Screen name="Register" component={Register} />
            <Stack.Screen name="Main" component={MainNavigatorTab} options={{ headerShown: false }} />
            <Stack.Screen name="Layanan" component={Layanan} />
            <Stack.Screen name="surat-usaha" component={SuratUsaha} options={{ title: 'Surat Usaha' }} />
            <Stack.Screen name="surat-kehilangan" component={SuratKehilangan} options={{ title: 'Surat Kehilangan' }} />
            <Stack.Screen name="surat-domisili" component={SuratDomisili} options={{ title: 'Surat Domisili' }} />
            <Stack.Screen name="surat-sanda" component={SuratSanda} options={{ title: 'Surat Sanda' }} />
            <Stack.Screen name="surat-nikah" component={SuratNikah} options={{ title: 'Surat Nikah' }} />
            <Stack.Screen name="surat-jual-putus" component={SuratJualPutus} options={{ title: 'Surat Jual Putus' }} />
            <Stack.Screen name="surat-penguasaan-fisik-tanah" component={SuratPenguasaanFisikTanah} options={{ title: 'Surat Penguasaan Fisik Tanah' }} />
            <Stack.Screen name="log-aktifitas" component={LogActivitas} options={{ title: 'Log Aktifitas' }} />
            <Stack.Screen name="Kontak" component={Kontak} options={{ title: 'Kontak' }} />
            <Stack.Screen name="Info" component={Info} options={{ title: 'Info' }} />
        </Stack.Navigator>
    )
}

export default Router;
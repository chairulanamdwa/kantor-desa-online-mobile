import info from './info.png';
import inovasi from './inovasi.png';
import kontak from './kontak.png';
import layanan from './layanan.png';
import LogAktifitas from './log-activitas.png';
import pengaduan from './pengaduan.png';

export {
    info,
    inovasi,
    kontak,
    layanan,
    LogAktifitas,
    pengaduan
}
import axios from 'axios';

export const loginApi = (data) => (desptch) => {
    desptch({ type: 'IS_LOADING', value: true });
    return new Promise((resolve, reject) => {
        (
            axios.post(`http://kantordesaonline.masuk.id/api/v1/user/login`, {
                'nik': data.nik,
                'password': data.password,
            }).then(res => {
                const dataUser = {
                    idUser: res.data.user.id,
                    tokenUser: res.data.access_token
                }
                desptch({ type: 'IS_LOADING', value: false });
                desptch({ type: 'LOGIN_TO_API', value: dataUser })
                resolve(true)
            }).catch(
                function (err) {
                    desptch({ type: 'IS_LOADING', value: false });
                    reject(false)
                }
            )
        )
    })
}

export const getUserApi = (data) => (dispatch) => {
    return new Promise((resolve, reject) => {
        (
            axios.get(`http://kantordesaonline.masuk.id/api/v1/user/getUser/` + data.user, { headers: { Authorization: data.AuthStr } }).then(res => {
                const dataUser = res.data;
                dispatch({ type: 'GET_USER_API', value: dataUser });
                resolve(true);
            }
            ).catch(
                function (err) {
                    reject(false);
                }
            )
        )
    })
}

export const getJenisSurat = () => (dispatch) => {
    dispatch({ type: 'IS_LOADING', value: true });
    return new Promise((resolve, reject) => {
        (
            axios.get(`http://kantordesaonline.masuk.id/api/v1/jenis-surat`).then(res => {
                const dataSurat = res.data.jenis_surat
                dispatch({ type: 'IS_LOADING', value: false });
                dispatch({ type: 'JENIS_SURAT', value: dataSurat })
                resolve(true);
            }).catch(
                function (err) {
                    dispatch({ type: 'IS_LOADING', value: false });
                    console.warn(err);
                    reject(false);
                }
            )

        )
    })
}

export const getBerita = () => (dispatch) => {
    dispatch({ type: 'IS_LOADING', value: true });
    return new Promise((resolve, reject) => {
        (
            axios.get(`http://newsapi.org/v2/top-headlines?country=id&apiKey=268402c6d3184137988c282fd4a331bb`).then(res => {
                const dataSurat = res.data.articles
                // console.warn(dataSurat)
                dispatch({ type: 'IS_LOADING', value: false });
                dispatch({ type: 'GET_BERITA_API', value: dataSurat })
                resolve(true);
            }).catch(
                function (err) {
                    dispatch({ type: 'IS_LOADING', value: false });
                    console.warn(err);
                    reject(false);
                }
            )

        )
    })
}


export const getKontak = () => (dispatch) => {
    dispatch({ type: 'IS_LOADING', value: true });
    return new Promise((resolve, reject) => {
        (
            axios.get(`http://kantordesaonline.masuk.id/api/v1/kontak`).then(res => {
                const dataSurat = res.data.kontak
                dispatch({ type: 'IS_LOADING', value: false });
                dispatch({ type: 'KONTAK', value: dataSurat })
                resolve(true);
            }).catch(
                function (err) {
                    dispatch({ type: 'IS_LOADING', value: false });
                    console.warn(err);
                    reject(false);
                }
            )

        )
    })
}

export const getNIKAPI = (data) => (dispatch) => {
    return new Promise((resolve, reject) => {
        (
            axios.post(`http://kantordesaonline.masuk.id/api/v1/penduduk`, {
                'nik': data.nik,
            }).then(res => {
                const dataPenduduk = res.data.penduduk;
                dispatch({ type: 'PENDUDUK', value: dataPenduduk });
                resolve(true)
            }).catch(
                function (err) {
                    reject(false)
                }
            )

        )
    })
}

export const GetAktifitas = (data) => (dispatch) => {
    dispatch({ type: 'IS_LOADING', value: true });
    return new Promise((resolve, reject) => {
        (
            axios.post(`http://kantordesaonline.masuk.id/api/v1/log-aktifitas`, {
                'id_user': data.id_user,
            }).then(res => {
                const logAktif = res.data.aktifitas.data;

                dispatch({ type: 'AKTIFITASLOG', value: logAktif });
                resolve(true)
                dispatch({ type: 'IS_LOADING', value: false });
            }).catch(
                function (err) {
                    reject(false)
                    dispatch({ type: 'IS_LOADING', value: false });
                }
            )

        )
    })
}

export const PostAktifitasKaryawan = (data) => (dispatch) => {
    dispatch({ type: 'IS_LOADING', value: true });
    return new Promise((resolve, reject) => {
        (
            axios.post(`http://kantordesaonline.masuk.id/api/v1/aktifitas`, {
                'jenis_aktifitas': data.jenis_aktifitas,
                'aktifitas': data.aktifitas,
                'tanggal': data.tanggal,
                'id_user': data.id_user,
            }).then(res => {
                dispatch({ type: 'IS_LOADING', value: false });
                resolve(true)
            }).catch(
                function (err) {
                    dispatch({ type: 'IS_LOADING', value: false });
                    reject(false)
                }
            )

        )
    })
}

export const ShowAktifitasKaryawan = (data) => (dispatch) => {
    dispatch({ type: 'IS_LOADING', value: true });
    return new Promise((resolve, reject) => {
        (
            axios.get(`http://kantordesaonline.masuk.id/api/v1/` + data.id_user + `/aktifitas-show`, {
                'id_user': 3,
            }).then(res => {
                const aktifitas = res.data.aktifitas;
                dispatch({ type: 'IS_LOADING', value: false });
                dispatch({ type: 'AKTIFITASKARYAWAN', value: aktifitas });
                resolve(true)
            }).catch(
                function (err) {
                    dispatch({ type: 'IS_LOADING', value: false });
                    reject(false)
                }
            )

        )
    })
}

export const DeleteAktifitasKaryawan = (data) => (dispatch) => {
    return new Promise((resolve, reject) => {
        (
            axios.post(`http://kantordesaonline.masuk.id/api/v1/aktifitas/delete`, {
                'id_user': data.id_user,
                'id_aktifitas': data.id_aktifitas
            }).then(res => {
                resolve(true)
            }).catch(
                function (err) {
                    dispatch({ type: 'IS_LOADING', value: false });
                    reject(false)
                }
            )

        )
    })
}

export const AjukanAktifitasKaryawan = (data) => (dispatch) => {
    return new Promise((resolve, reject) => {
        (
            axios.post(`http://kantordesaonline.masuk.id/api/v1/aktifitas/ajukan`, {
                'id_user': data.id_user,
            }).then(res => {
                const aktifitas = res.data.aktifitas;
                resolve(true)
            }).catch(
                function (err) {
                    dispatch({ type: 'IS_LOADING', value: false });
                    reject(false)
                }
            )

        )
    })
}
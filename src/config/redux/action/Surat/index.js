import axios from 'axios';

export const PostSuratUsaha = (data) => (desptch) => {
    desptch({ type: 'IS_LOADING', value: true });
    return new Promise((resolve, reject) => {
        (

            axios.post(`http://kantordesaonline.masuk.id/api/v1/surat-usaha-post`, {
                'id_surat': '1',
                'id_penduduk1': data.id_penduduk1,
                'usaha': data.usaha,
                'id_user': data.id_user,
            }).then(res => {

                const data = {
                    code: res.data.code,
                    message: res.data.message,
                }
                desptch({ type: 'IS_LOADING', value: false });
                desptch({ type: 'KIRIMSURAT', value: data });
                resolve(true)
            }).catch(
                function (err) {
                    // console.warn(err);
                    desptch({ type: 'IS_LOADING', value: false });
                    reject(false)
                }
            )
        )
    })
}

export const PostSuratKehilangan = (data) => (desptch) => {
    desptch({ type: 'IS_LOADING', value: true });
    return new Promise((resolve, reject) => {
        (
            axios.post(`http://kantordesaonline.masuk.id/api/v1/surat-kehilangan-post`, {
                'id_surat': '2',
                'id_penduduk1': data.id_penduduk1,
                'barang': data.barang,
                'tgl_hilang': '2020-06-16',
                'tmpt_hilang': data.tmpt_hilang,
                'menuju': data.menuju,
                'telpon': data.telpon,
                'id_user': data.id_user,
            }).then(res => {
                const data = {
                    code: res.data.code,
                    message: res.data.message,
                }
                desptch({ type: 'IS_LOADING', value: false });
                desptch({ type: 'KIRIMSURAT', value: data });
                resolve(true)
            }).catch(
                function (err) {
                    desptch({ type: 'IS_LOADING', value: false });
                    reject(false)
                }
            )
        )
    })
}

export const PostSuratDomisili = (data) => (desptch) => {
    desptch({ type: 'IS_LOADING', value: true });
    return new Promise((resolve, reject) => {
        (
            axios.post(`http://kantordesaonline.masuk.id/api/v1/surat-domisili-post`, {
                'id_surat': '3',
                'id_penduduk1': data.id_penduduk1,
                'id_user': data.id_user,
            }).then(res => {
                const data = {
                    code: res.data.code,
                    message: res.data.message,
                }
                desptch({ type: 'IS_LOADING', value: false });
                desptch({ type: 'KIRIMSURAT', value: data });
                resolve(true)
            }).catch(
                function (err) {
                    // console.warn(err);
                    desptch({ type: 'IS_LOADING', value: false });
                    reject(false)
                }
            )
        )
    })
}

export const PostSuratSanda = (data) => (desptch) => {
    desptch({ type: 'IS_LOADING', value: true });
    return new Promise((resolve, reject) => {
        (
            axios.post(`http://kantordesaonline.masuk.id/api/v1/surat-sanda-post`, {
                'id_surat': '3',
                'id_penduduk1': data.id_penduduk1,
                'id_penduduk2': data.id_penduduk2,
                'kata_harga': data.kata_harga,
                'harga': data.harga,
                'keterangan': data.keterangan,
                'saksi1': data.saksi1,
                'saks2': data.saks2,
                'barat': data.barat,
                'timur': data.timur,
                'utara': data.utara,
                'selatan': data.selatan,
                'id_user': data.id_user,
            }).then(res => {
                const data = {
                    code: res.data.code,
                    message: res.data.message,
                }
                desptch({ type: 'IS_LOADING', value: false });
                desptch({ type: 'KIRIMSURAT', value: data });
                resolve(true)
            }).catch(
                function (err) {
                    // console.warn(err);
                    desptch({ type: 'IS_LOADING', value: false });
                    reject(false)
                }
            )
        )
    })
}
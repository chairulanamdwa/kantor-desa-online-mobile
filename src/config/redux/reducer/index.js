const initialState = {
    isLogin: true,
    isLoading: false,
    tokenUser: '',
    idUser: '',
    callbackSurat: '',
    listSurat: [],
    listKontak: [],
    listBerita: [],
    user: {
        name: 'Nama User',
        nik: 'NIK User'
    },
    penduduk: {},
    logAktifitas: [],
    aktifitasKaryawan: [],
}

const reducer = (state = initialState, action) => {
    if (action.type === 'LOGIN_TO_API') {
        return {
            ...state,
            tokenUser: action.value.tokenUser,
            idUser: action.value.idUser
        }
    }
    if (action.type === 'GET_USER_API') {
        return {
            ...state,
            user: action.value
        }
    }
    if (action.type === 'IS_LOADING') {
        return {
            ...state,
            isLoading: action.value
        }
    }
    if (action.type === 'IS_LOGIN') {
        return {
            ...state,
            isLogin: action.value
        }
    }
    if (action.type === 'JENIS_SURAT') {
        return {
            ...state,
            listSurat: action.value
        }
    }
    if (action.type === 'KONTAK') {
        return {
            ...state,
            listKontak: action.value
        }
    }
    if (action.type === 'PENDUDUK') {
        return {
            ...state,
            penduduk: action.value
        }
    }
    if (action.type === 'KIRIMSURAT') {
        return {
            ...state,
            callbackSurat: action.value
        }
    }
    if (action.type === 'AKTIFITASLOG') {
        return {
            ...state,
            logAktifitas: action.value
        }
    }
    if (action.type === 'AKTIFITASKARYAWAN') {
        return {
            ...state,
            aktifitasKaryawan: action.value
        }
    }
    if (action.type === 'GET_BERITA_API') {
        return {
            ...state,
            listBerita: action.value
        }
    }

    return state;
}


export default reducer;
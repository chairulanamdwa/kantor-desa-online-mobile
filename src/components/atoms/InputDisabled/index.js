import React from 'react';
import { StyleSheet, TextInput, View } from 'react-native';

const InputDisabled = ({ ...props }) => {
    return (
        <TextInput style={styles.input} {...props} placeholderTextColor="#fff" />
    );
}

export default InputDisabled;

const styles = StyleSheet.create({
    input: {
        borderColor: '#ddd',
        borderRadius: 5,
        borderWidth: 1,
        color: '#fff',
        height: 40,
        marginVertical: 5,
        paddingHorizontal: 20,
        width: '100%',
        backgroundColor: '#ccc'
    }
})
import React from 'react';
import { StyleSheet, Text, TextInput, TouchableOpacity, View } from 'react-native';

const CButton = ({ status, title, ...props }) => {
    if (status) {
        return (
            <View style={styles.isLoading} >
                <Text style={styles.textButton}>Loading...</Text>
            </View>
        );
    }
    return (
        <TouchableOpacity style={styles.button} {...props}>
            <Text style={styles.textButton}>{title}    </Text>
        </TouchableOpacity>
    );
}

export default CButton;
const styles = StyleSheet.create({
    button: {
        alignItems: 'center',
        backgroundColor: '#E29700',
        borderRadius: 5,
        elevation: 8,
        height: 40,
        marginVertical: 30,
        padding: 10,
        shadowColor: '#E29700',
        width: '100%',
    },
    isLoading: {
        alignItems: 'center',
        backgroundColor: '#a3a3a3',
        borderRadius: 5,
        height: 40,
        marginVertical: 30,
        padding: 10,
        width: '100%',
    },
    textButton: {
        color: '#FFFFFF'
    }
});
import React from 'react';
import { StyleSheet, TextInput, View } from 'react-native';
import DatePicker from 'react-native-datepicker';

const InputDate = ({ ...props }) => {
    return (
        <DatePicker
            style={styles.inpurDate}
            date={new Date()}
            mode="date"
            placeholder="select date"
            format="YYYY-MM-DD"
            minDate="2016-05-01"
            confirmBtnText="SIP"
            cancelBtnText="Cancel"
        />
    );
}

export default InputDate;

const styles = StyleSheet.create({
    inpurDate: {
        height: 40,
        marginVertical: 20,
        width: '100%',
    }
})
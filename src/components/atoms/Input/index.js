import React from 'react';
import { StyleSheet, TextInput, View } from 'react-native';

const Input = ({ colorBorder = '#E29700', ...props }) => {
    return (
        <TextInput style={{
            borderColor: colorBorder,
            borderRadius: 5,
            borderWidth: 1,
            color: '#E29700',
            height: 40,
            marginVertical: 5,
            paddingHorizontal: 20,
            width: '100%',
        }} {...props} placeholderTextColor="#E29700" />
    );
}

export default Input;

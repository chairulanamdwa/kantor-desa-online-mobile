import React from 'react';
import { StyleSheet, TextInput } from 'react-native';

const Textarea = ({ ...props }) => {
    return (
        <TextInput style={styles.input} {...props} numberOfLines={10} multiline={true} placeholderTextColor="#E29700" />
    );
}

export default Textarea;

const styles = StyleSheet.create({
    input: {
        borderColor: '#E29700',
        borderRadius: 5,
        borderWidth: 1,
        color: '#E29700',
        height: 125,
        textAlignVertical: 'top',
        paddingHorizontal: 20,
        width: '100%',
    }
})